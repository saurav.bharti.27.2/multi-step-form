
let page1= document.getElementById('form_layout_1')
let page2= document.getElementById('form_layout_2')
let page3= document.getElementById('form_layout_3')
let page4= document.getElementById('form_layout_4')
let page5= document.getElementById('form_layout_5')

let array_of_pages = [page1, page2, page3, page4, page5];

let array_of_functions = [render_page_1, render_page_2, render_page_3, render_page_4];
let array_of_information = []; 
let plan_type_variable = 'arcade';
let plan = 'monthly';

let selected_card = [true, true, false];

let event_listeners_of_cards = [false, false, false]
let flag_forward_1 = false, flag_forward_2 = false, flag_backward_2 = false, flag_forward_3 = false, flag_backward_3 = false, flag_forward_4 = false, flag_backward_4 = false;

function render_pages(page_counter){
    array_of_pages.forEach((page, index)=> {
        let style_id = `stylesheet_page_${index+1}`
    
        if((index+1 )== page_counter){
            document.getElementById(style_id).disabled = false;
            
            page.style.display = "flex"

            if(index<4){
                array_of_functions[index]()
            }
        }
        else{
            document.getElementById(style_id).disabled = true;
            page.style.display = "none"
        }   
    })

}
document.addEventListener('DOMContentLoaded', ()=>{
    render_pages(1)
})


function render_page_1(){
    let name = document.querySelector('#name_input')
        let email = document.querySelector('#email_input')
        let phone_number = document.querySelector('#phone_number')
    
        let name_error_label = document.querySelector('.label_message_name')
        let email_error_label = document.querySelector('.label_message_email')
        let phone_error_label = document.querySelector('.label_message_phone')
    
        let form_button = document.querySelector('#form_btn');
    
        let regex_for_email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        let regex_for_phone = /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
    
    
        document.addEventListener('DOMContentLoaded', function(){
            array_of_information = []
        })
        
        if(array_of_information.length > 0 ){

            let array_of_previous_details = array_of_information
    
            name.value = array_of_previous_details[0]
            email.value = array_of_previous_details[1]
            phone_number.value = array_of_previous_details[2]
        }
        
    
        form_button.addEventListener('click', function(){
            
            array_of_information = [name.value, email.value, phone_number.value]
            
            let error_flag =0
    
            if(!phone_number.value){
                phone_error_label.style.display = 'block'
                phone_error_label.border = "0.1rem solid red"
                phone_number.focus()
    
                phone_number.style.border = "0.1rem solid red"
    
                error_flag=1;
            }else if(!phone_number.value.match(regex_for_phone)){
                phone_error_label.style.display = "block"
                phone_error_label.innerText = "Please enter valid Phone Number"
                phone_number.style.border = "0.1rem solid red"
                phone_number.focus()
    
                error_flag=1;
            }else{
                phone_error_label.style.display = 'none'
            }
    
            
            let string_email = String(email.value)
            if(!email.value){
                email_error_label.style.display = "block"
                email.style.border = "0.1rem solid red"
                email.focus()
    
                error_flag=1;
            }else if(!string_email.toLowerCase().match(regex_for_email)){
                email_error_label.style.display = "block"
                email_error_label.innerText = "Please enter valid email"
                email.style.border = "0.1rem solid red"
                email.focus()
    
                error_flag=1;
            }
            else{
                email_error_label.style.display= 'none'
            }
            
            if(!name.value){
                name_error_label.style.display="block"
                name.style.border = "0.1rem solid red";
                name.focus()
    
                error_flag=1;
            }
            else{
                name_error_label.style.display = 'none'
            }
    
            if(error_flag){
                execute_for_errors()
                return;
            }
            else{
                render_pages(2)
            }
        })
    
        let all_input = document.querySelectorAll('input')
    
    
        function execute_for_errors(){
            
            all_input.forEach((input) => {
                
                input.addEventListener('blur', function(event){
                    event.preventDefault();
                    event.target.style.border =  '0.1rem solid hsl(231, 11%, 63%)'
                })
                input.addEventListener('click', (event) => {
                    event.preventDefault()
                    event.target.style.border = '0.1rem solid hsl(243, 100%, 62%)'; 
                })
            })
        }
}

function render_page_2(){
    let toggler = document.querySelector('.slider');
    let monthly = document.querySelector('.monthly')
    let yearly = document.querySelector('.yearly')
    let counter = 0;
    
    let flag_for_childs = 0;
    let initial_plan = plan;
    
    if(initial_plan == 'yearly'){
    
        counter+= 1;
    
        let make_it_slide = document.querySelector('#checkbox_btn')
        make_it_slide.checked = true;
    }
    
    // rendering the child
    initial_stage_of_childs(counter)
    
    toggler.addEventListener('click', function(){ 
    
        counter += 1;
     
        initial_stage_of_childs(counter)
    })
    
    function initial_stage_of_childs(counter){
    
        let arcade_price = document.querySelector('#arcade_plan_price')
        let advanced_price = document.querySelector('#advanced_plan_price')
        let pro_price = document.querySelector('#pro_plan_price')
        
        if(counter%2==1){
            yearly.className += ' active'   
    
            if(monthly.className.includes('active')){
                monthly.className = monthly.className.slice(0, -7) 
            }
            plan = 'yearly'
    
            arcade_price.innerHTML = '$90/yr'
            advanced_price.innerHTML = '$120/yr'
            pro_price.innerHTML = '$150/yr'
            
            let arcade_span = document.createElement('span')
            arcade_span.innerHTML = '2 months free'
            arcade_span.className = 'arcade_free_plan'
            arcade_span.id= 'arcade_free_plan_id'
        
            let advanced_span = document.createElement('span')
            advanced_span.innerHTML = '2 months free'
            advanced_span.className = 'advanced_free_plan'
        
            let pro_span = document.createElement('span')
            pro_span.innerHTML = '2 months free'
            pro_span.className = 'pro_free_plan'
            
            if(!document.getElementById('arcade_plan_information').contains(document.getElementById('arcade_free_plan_id'))){

                document.getElementById('arcade_plan_information').appendChild(arcade_span)
                document.getElementById('advanced_plan_information').appendChild(advanced_span)
                document.getElementById('pro_plan_information').appendChild(pro_span)
            }
            

            

            flag_for_childs = 1;
    
            document.querySelectorAll('.plan_information').forEach((ele) => ele.style.height = '32%'); 
            }
            else{
    
                monthly.className += ' active' 
                if(yearly.className.includes('active')){
                    yearly.className = yearly.className.slice(0, -7)
                }
    
                plan = 'monthly'
    
                arcade_price.innerHTML = '$9/mo'
                advanced_price.innerHTML = '$12/mo'
                pro_price.innerHTML = '$15/mo'
    
                if(flag_for_childs && document.getElementById('arcade_plan_information').contains(document.getElementById('arcade_free_plan_id'))){

                    document.getElementById('arcade_plan_information').removeChild(document.querySelector('.arcade_free_plan'))
                    document.getElementById('advanced_plan_information').removeChild(document.querySelector('.advanced_free_plan'))
                    document.getElementById('pro_plan_information').removeChild(document.querySelector('.pro_free_plan'))

                }
                
    
                document.querySelectorAll('.plan_information').forEach((ele) => ele.style.height = '26%'); 
            }
        }
    
        plan_type = plan_type_variable
        document.querySelector(`#${plan_type}`).className += ' active';
    
        let plan_container = document.querySelectorAll('.plan_container').forEach((each_plan) => {
            each_plan.addEventListener('click', function() {
                document.querySelectorAll('.plan_container').forEach((iterating_each_task) => {
    
                    if(iterating_each_task.className.includes('active')){
                        iterating_each_task.className= iterating_each_task.className.slice(0, -7)
                    }
                })
    
                
                plan_type_variable = each_plan.id
                if(!each_plan.className.includes(' active')){
                    each_plan.className += ' active'
                }
    
            })
        })

        if(!flag_backward_2){
            flag_backward_2 = true;

            document.getElementById('btn_backward_page2').onclick = function(){
                render_pages(1)
            }
        }

        if(!flag_forward_2){
            flag_forward_2 = true;
            document.getElementById('btn_forward_page2').onclick = function(){
                render_pages(3)
            }
        }
    
}

function render_page_3(){
    let all_cards = document.querySelectorAll('.card')

        let added_cards = selected_card

        if(added_cards){
            
            all_cards.forEach((card, index) => {

                let checkbox = card.querySelector('#add_on_checkbox')

                if(added_cards[index]){
                    if(!card.className.includes('active')){
                        card.className += ' active'
                    }

                    checkbox.checked = true
                }
            })
        }

        all_cards.forEach((card, index) => {

            if(!event_listeners_of_cards[index]){

                event_listeners_of_cards[index] = true;

                card.addEventListener('click', function(){
    
                    if(card.className.includes('active')){
    
                        let temp = card.className
                        card.className = temp.slice(0, -7)
                    }else{
                        if(!card.className.includes('active')){
                            card.className += ' active'
                        }
                    }
                    
                    load_selected_features()
    
                    
                })

            }
        })


        function load_selected_features(){
            all_cards.forEach((individual_card, index) => {
                
                let checkbox = individual_card.querySelector('#add_on_checkbox')
            
                if(individual_card.className.includes('active')){
                    checkbox.checked = true;
                    added_cards[index] = true;
                }else{
                    checkbox.checked = false;
                    added_cards[index] = false;
                }
                selected_card = added_cards

            })
        }

        let type_of_plan = plan
        let monthly_price = [1, 2, 2]
        let yearly_price = [10, 20, 20]

        document.querySelectorAll('.feature_price').forEach((service,index) => {
                service.innerText = `+$${type_of_plan == 'monthly' ? monthly_price[index] :yearly_price[index]}/${type_of_plan=="monthly" ? 'mo' : 'yr'}`
        })

        if(!flag_backward_3){
            flag_backward_3 = true
            document.getElementById('btn_backward_page3').addEventListener( 'click', function(){
                render_pages(2)
            })
        }

        if(!flag_forward_3){
            flag_forward_3 = true
            document.getElementById('btn_forward_page3').addEventListener('click', function(){
                render_pages(4)
            })
        }


}

function render_page_4(){
    const monthly_plan = [9, 12, 15]
    const yearly_plan = [90, 120, 150]

    const available_plan = ['arcade', 'advanced', 'pro']

    const monthly_features = [ ['Online service', 1], ['Larger storage', 2], ['Customizable profile', 3]]

    const yearly_features = [['Online service', 10], ['Larger storage', 20], ['Customizable profile', 20]]

    const price_counter = []

    let plan_type = plan_type_variable

    let features_selected = selected_card

    if(plan_type){
        
        let parent_plan_cost = document.querySelector(".plan_selected_div")
        while(parent_plan_cost.firstChild){
            parent_plan_cost.removeChild(parent_plan_cost.firstChild)
        }


        let plan_information_top = document.createElement('h4')
        plan_information_top.className = 'plan_information_top'

        let here_plan_type = plan_type[0].toUpperCase() + plan_type.substring(1)
        let here_plan = plan[0].toUpperCase()  + plan.substring(1)


        let change_plan = document.createElement('div')
        change_plan.className = 'change_plan'
        let change_button = document.createElement('a')
        change_button.innerText = 'Change'
        change_button.onclick = function(){
            render_pages(2)
        }


        change_plan.appendChild(change_button)
        parent_plan_cost.appendChild(change_plan);

        plan_information_top.innerText = `${here_plan_type} (${here_plan})`
        document.querySelector('.change_plan').appendChild(plan_information_top)
        
        let index_of_plan = available_plan.indexOf(plan_type)

        let price = 0;
        if(plan == 'monthly'){
            price = monthly_plan[index_of_plan]
        }else{
            price = yearly_plan[index_of_plan]
        }
        price_counter.push(price)

        let plan_cost = document.createElement('h4');
        plan_cost.className = 'plan_cost'



        plan_cost.innerText = `$${price}/${plan=="monthly"? "mo" : "yr"}`
        document.querySelector(".plan_selected_div").appendChild(plan_cost)
    }
    
    
    
    if(features_selected){

            let parent_of_feature = document.querySelector('.features_added_div')
            while(parent_of_feature.firstChild){
                parent_of_feature.removeChild(parent_of_feature.firstChild)
            }

            features_selected.map((feature, index) => {


                if(feature){
                    let service_feature = document.createElement('div')
                    service_feature.className = 'service_feature'

                    let service_feature_plan = document.createElement('span')
                    let service_feature_cost = document.createElement('span')

                    service_feature_plan.className = 'service_feature_plan'
                    service_feature_cost.className = 'service_feature_cost'
                    
                    let cost_of_feature = (plan == 'monthly' ? monthly_features[index] : yearly_features[index])
                    price_counter.push(cost_of_feature[1])

                    service_feature_plan.innerText = `${cost_of_feature[0]}`
                    service_feature_cost.innerText = `$${cost_of_feature[1]}/${plan=="monthly" ? 'mo' : 'yr'}`
                    
                    service_feature.appendChild(service_feature_plan)
                    service_feature.appendChild(service_feature_cost)

                    document.querySelector('.features_added_div').appendChild(service_feature)
                }
            })
        }

        if(price_counter){

            let total_price = 0;
            price_counter.forEach((value) => total_price+= value)

            let total_price_label = document.createElement('span')
            total_price_label.className = 'total_price_label'
            total_price_label.innerText = `Total (per ${plan == "monthly" ? "month" : "year"})`
            
            let total_price_preview = document.createElement('h3')
            total_price_preview.className = 'total_price_preview'
            total_price_preview.innerText = `$${total_price}/${plan=="monthly" ? "mo" : "yr"}`

            let parent_total_cost_div = document.querySelector('.total_cost_div');

            while(parent_total_cost_div.firstChild){
                parent_total_cost_div.removeChild(parent_total_cost_div.firstChild)
            }
        
            document.querySelector('.total_cost_div').appendChild(total_price_label)
            document.querySelector('.total_cost_div').appendChild(total_price_preview)
        }

        
        if(!flag_backward_4){
            flag_backward_4 = true
            document.getElementById('btn_backward_page4').addEventListener('click', function(){
                render_pages(3)
            })
        }

        if(!flag_forward_4){
            flag_forward_4 = true
            document.getElementById('btn_forward_page4').addEventListener('click', function(){
                render_pages(5)
            })
        }


}


