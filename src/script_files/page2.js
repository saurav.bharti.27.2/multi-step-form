
let toggler = document.querySelector('.slider');
let monthly = document.querySelector('.monthly')
let yearly = document.querySelector('.yearly')
let counter = 0;

let flag_for_childs = 0;
let initial_plan;

if(!localStorage.getItem('plan')){
    localStorage.setItem('plan', 'monthly')
    initial_plan = 'monthly'
}else{
    initial_plan = localStorage.getItem('plan')
}

if(!localStorage.getItem('plan-type')){
    localStorage.setItem('plan-type', "arcade")
}

if(initial_plan == 'yearly'){

    counter+= 1;

    let make_it_slide = document.querySelector('#checkbox_btn')
    make_it_slide.checked = true;
}

// rendering the child
initial_stage_of_childs(counter)

toggler.addEventListener('click', function(){ 

    counter += 1;
 
    initial_stage_of_childs(counter)
})



function initial_stage_of_childs(counter){

    let arcade_price = document.querySelector('#arcade_plan_price')
    let advanced_price = document.querySelector('#advanced_plan_price')
    let pro_price = document.querySelector('#pro_plan_price')
    
    if(counter%2==1){
        yearly.className += ' active'

        if(monthly.className.includes('active')){
            monthly.className = monthly.className.slice(0, -7) 
        }
        localStorage.setItem('plan', "yearly")

        arcade_price.innerHTML = '$90/yr'
        advanced_price.innerHTML = '$120/yr'
        pro_price.innerHTML = '$150/yr'
        
        let arcade_span = document.createElement('span')
        arcade_span.innerHTML = '2 months free'
        arcade_span.className = 'arcade_free_plan'
    
        let advanced_span = document.createElement('span')
        advanced_span.innerHTML = '2 months free'
        advanced_span.className = 'advanced_free_plan'
    
        let pro_span = document.createElement('span')
        pro_span.innerHTML = '2 months free'
        pro_span.className = 'pro_free_plan'
        
        document.getElementById('arcade_plan_information').appendChild(arcade_span)
        document.getElementById('advanced_plan_information').appendChild(advanced_span)
        document.getElementById('pro_plan_information').appendChild(pro_span)

        flag_for_childs = 1;

        document.querySelectorAll('.plan_information').forEach((ele) => ele.style.height = '32%'); 
    }
    else{

        monthly.className += ' active' 
        if(yearly.className.includes('active')){
            yearly.className = yearly.className.slice(0, -7)
        }

        localStorage.setItem('plan', 'monthly')

        arcade_price.innerHTML = '$9/mo'
        advanced_price.innerHTML = '$12/mo'
        pro_price.innerHTML = '$15/mo'

        if(flag_for_childs){
            document.getElementById('arcade_plan_information').removeChild(document.querySelector('.arcade_free_plan'))
            document.getElementById('advanced_plan_information').removeChild(document.querySelector('.advanced_free_plan'))
            document.getElementById('pro_plan_information').removeChild(document.querySelector('.pro_free_plan'))
        }
        

        document.querySelectorAll('.plan_information').forEach((ele) => ele.style.height = '26%'); 
    }
}

let plan_type = localStorage.getItem('plan-type')
document.querySelector(`#${plan_type}`).className += ' active';

let plan_container = document.querySelectorAll('.plan_container').forEach((each_plan) => {
    each_plan.addEventListener('click', function() {
        document.querySelectorAll('.plan_container').forEach((iterating_each_task) => {

            if(iterating_each_task.className.includes('active')){
                iterating_each_task.className= iterating_each_task.className.slice(0, -7)
            }
        })

        localStorage.setItem('plan-type', each_plan.id)
        each_plan.className += ' active'

    })
})


