
let all_cards = document.querySelectorAll('.card')

let added_cards = [true, true, false]

if(!localStorage.getItem("select-cards")){
    localStorage.setItem("select-cards", JSON.stringify(added_cards))
}
else{
    added_cards = JSON.parse(localStorage.getItem("select-cards"))
}
console.log(added_cards)

if(added_cards){
    
    all_cards.forEach((card, index) => {

        let checkbox = card.querySelector('#add_on_checkbox')

        if(added_cards[index]){
            card.className += ' active'
            checkbox.checked = true
        }
    })
}

all_cards.forEach((card) => {

    card.addEventListener('click', function(){

        if(card.className.includes('active')){

            let temp = card.className
            card.className = temp.slice(0, -7)
        }else{
            card.className += ' active'
        }

        load_selected_features()

        
    })
})


function load_selected_features(){
    all_cards.forEach((individual_card, index) => {
        
        let checkbox = individual_card.querySelector('#add_on_checkbox')
    
        if(individual_card.className.includes('active')){
            checkbox.checked = true;
            added_cards[index] = true;
        }else{
            checkbox.checked = false;
            added_cards[index] = false;
        }

        localStorage.setItem("select-cards", JSON.stringify(added_cards))

    })
}

let type_of_plan = localStorage.getItem('plan')
let monthly_price = [1, 2, 2]
let yearly_price = [10, 20, 20]

document.querySelectorAll('.feature_price').forEach((service,index) => {
        service.innerText = `+$${type_of_plan == 'monthly' ? monthly_price[index] :yearly_price[index]}/${type_of_plan=="monthly" ? 'mo' : 'yr'}`
})

