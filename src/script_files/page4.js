
const monthly_plan = [9, 12, 15]
const yearly_plan = [90, 120, 150]

const available_plan = ['arcade', 'advanced', 'pro']

const monthly_features = [ ['Online service', 1], ['Larger storage', 2], ['Customizable profile', 3]]

const yearly_features = [['Online service', 10], ['Larger storage', 20], ['Customizable profile', 20]]

const price_counter = []

let plan_type = localStorage.getItem('plan-type')
let plan = localStorage.getItem('plan')

let features_selected_array = localStorage.getItem('select-cards')
let features_selected = JSON.parse(features_selected_array)

if(plan_type){

    let plan_information_top = document.createElement('h4')
    plan_information_top.className = 'plan_information_top'

    let here_plan_type = plan_type[0].toUpperCase() + plan_type.substring(1)
    let here_plan = plan[0].toUpperCase()  + plan.substring(1)

    plan_information_top.innerText = `${here_plan_type} (${here_plan})`
    document.querySelector('.change_plan').appendChild(plan_information_top)
    
    let index_of_plan = available_plan.indexOf(plan_type)

    let price = 0;
    if(plan == 'monthly'){
        price = monthly_plan[index_of_plan]
    }else{
        price = yearly_plan[index_of_plan]
    }
    price_counter.push(price)

    let plan_cost = document.createElement('h4');
    plan_cost.className = 'plan_cost'

    plan_cost.innerText = `$${price}/${plan=="monthly"? "mo" : "yr"}`
    document.querySelector(".plan_selected_div").appendChild(plan_cost)
}

if(features_selected){
    console.log(features_selected, typeof features_selected)
    features_selected.map((feature, index) => {

        if(feature){
            let service_feature = document.createElement('div')
            service_feature.className = 'service_feature'

            let service_feature_plan = document.createElement('span')
            let service_feature_cost = document.createElement('span')

            service_feature_plan.className = 'service_feature_plan'
            service_feature_cost.className = 'service_feature_cost'
            
            let cost_of_feature = (plan == 'monthly' ? monthly_features[index] : yearly_features[index])
            price_counter.push(cost_of_feature[1])

            service_feature_plan.innerText = `${cost_of_feature[0]}`
            service_feature_cost.innerText = `$${cost_of_feature[1]}/${plan=="monthly" ? 'mo' : 'yr'}`
            
            service_feature.appendChild(service_feature_plan)
            service_feature.appendChild(service_feature_cost)

            document.querySelector('.features_added_div').appendChild(service_feature)
        }
    })
}

if(price_counter){

    let total_price = 0;
    price_counter.forEach((value) => total_price+= value)

    let total_price_label = document.createElement('span')
    total_price_label.className = 'total_price_label'
    total_price_label.innerText = `Total (per ${plan == "monthly" ? "month" : "year"})`
    
    let total_price_preview = document.createElement('h3')
    total_price_preview.className = 'total_price_preview'
    total_price_preview.innerText = `$${total_price}/${plan=="monthly" ? "mo" : "yr"}`

    document.querySelector('.total_cost_div').appendChild(total_price_label)
    document.querySelector('.total_cost_div').appendChild(total_price_preview)
}




