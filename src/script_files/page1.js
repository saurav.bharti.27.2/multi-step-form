
let name = document.querySelector('#name_input')
let email = document.querySelector('#email_input')
let phone_number = document.querySelector('#phone_number')

let name_error_label = document.querySelector('.label_message_name')
let email_error_label = document.querySelector('.label_message_email')
let phone_error_label = document.querySelector('.label_message_phone')

let form_button = document.querySelector('#form_btn');

let regex_for_email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
let regex_for_phone = /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;


document.addEventListener('DOMContentLoaded', function(){
    localStorage.clear()
})

if(localStorage.getItem('details')){
    let array_of_previous_details = JSON.parse(localStorage.getItem('details'))
    name.value = array_of_previous_details[0]
    email.value = array_of_previous_details[1]
    phone_number.value = array_of_previous_details[2]
}

form_button.addEventListener('click', function(){
    
    let array_of_values = [name.value, email.value, phone_number.value]
    
    if(!localStorage.getItem('details')){
        localStorage.setItem('details', JSON.stringify(array_of_values))
    }
    else{
        console.log(array_of_values, "now")
        localStorage.setItem('details', JSON.stringify(array_of_values))
        
    }
    console.log(array_of_values, "later")

    let error_flag =0

    if(!phone_number.value){
        phone_error_label.style.display = 'block'
        phone_error_label.border = "0.1rem solid red"
        phone_number.focus()

        phone_number.style.border = "0.1rem solid red"

        error_flag=1;
    }else if(!phone_number.value.match(regex_for_phone)){
        phone_error_label.style.display = "block"
        phone_error_label.innerText = "Please enter valid Phone Number"
        phone_number.style.border = "0.1rem solid red"
        phone_number.focus()

        error_flag=1;
    }else{
        phone_error_label.style.display = 'none'
    }

    
    let string_email = String(email.value)
    if(!email.value){
        email_error_label.style.display = "block"
        email.style.border = "0.1rem solid red"
        email.focus()

        error_flag=1;
    }else if(!string_email.toLowerCase().match(regex_for_email)){
        email_error_label.style.display = "block"
        email_error_label.innerText = "Please enter valid email"
        email.style.border = "0.1rem solid red"
        email.focus()

        error_flag=1;
    }
    else{
        email_error_label.style.display= 'none'
    }
    
    if(!name.value){
        name_error_label.style.display="block"
        name.style.border = "0.1rem solid red";
        name.focus()

        error_flag=1;
    }
    else{
        name_error_label.style.display = 'none'
    }

    if(error_flag){
        execute_for_errors()
        return;
    }
    else{
        window.location.href= "./page2.html"
    }
})

let all_input = document.querySelectorAll('input')


function execute_for_errors(){
    
    all_input.forEach((input) => {
        
        input.addEventListener('blur', function(event){
            event.preventDefault();
            event.target.style.border =  '0.1rem solid hsl(231, 11%, 63%)'
        })
        input.addEventListener('click', (event) => {
            event.preventDefault()
            event.target.style.border = '0.1rem solid hsl(243, 100%, 62%)'; 
        })
    })
}